<?php
try {
  if (!empty($_POST['ticker']))
    file_put_contents('../ticker.json', $_POST['ticker']);
  else if (!empty($_POST['breves']))
    file_put_contents('../breves.json', $_POST['breves']);
  elseif (!empty($_POST['data']))
    file_put_contents('../live.html', json_decode($_POST['data']));
  else
    throw new Exception("no data");

  echo 'ok';
} catch (Exception $e) {
  echo 'oh no';
}
