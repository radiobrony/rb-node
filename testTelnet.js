/* eslint-disable */
const fs = require('fs')
const yaml = require('js-yaml')

const config = yaml.load(fs.readFileSync('config.yml', 'utf8'))

const { Telnet } = require('telnet-client')
const connection = new Telnet()

process.setMaxListeners(50)

// these parameters are just examples and most probably won't work for your use-case.
const params = {
  host: config.conn.LS_HOST,
  port: config.conn.LS_PORT,
  negotiationMandatory: false,
  timeout: 1500
}

function parseLiquidHistory (metadata) {
  const hist = []
  const lines = metadata.split(/\r\n|\n/)
  let hi = 0

  // Build our main array of objects
  const highest = parseInt(lines[0].substring(4, lines[0].length - 4), 10)
  for (let index = highest - 1; index >= 0; index--) {
    hist[index] = {
      title: '',
      artist: '',
      start: 0,
      length: 0,
      source: null
    }
  }

  // Parse our history
  for (let index = 0; index < lines.length; index++) {
    const line = lines[index]

    if (line.startsWith('--- ')) {
      // This is the count of how many tracks it was (+1)
      hi = line.substring(4, line.length - 4) - 1
    } else if (line.startsWith('title=')) {
      hist[hi].title = line.substring(7, line.length - 1)
    } else if (line.startsWith('artist=')) {
      hist[hi].artist = line.substring(8, line.length - 1)
    } else if (line.startsWith('on_air_timestamp=')) {
      hist[hi].start = line.substring(18, line.length - 4)
    } else if (line.startsWith('tlen=')) {
      hist[hi].length = line.substring(6, line.length - 1)
    } else if (line.startsWith('source=')) {
      hist[hi].source = line.substring(8, line.length - 1)
    }
  }

  // Return without duplicates
  return hist.filter((value, index, self) =>
    index === self.findIndex((t) => (
      t.start === value.start && t.title === value.title
    ))
  )
}

function parseLiquidMeta (metadata) {
  const lines = metadata.split(/\r\n|\n/)
  const meta = {
    title: '',
    artist: '',
    start: 0,
    length: 0,
    source: null
  }
  console.log('meta length:', metadata.length)

  // Parse our lines
  for (let index = 0; index < lines.length; index++) {
    const line = lines[index]

    if (line.startsWith('title=')) {
      meta.title = line.substring(7, line.length - 1)
    } else if (line.startsWith('artist=')) {
      meta.artist = line.substring(8, line.length - 1)
    } else if (line.startsWith('on_air_timestamp=')) {
      meta.start = line.substring(18, line.length - 4)
    } else if (line.startsWith('tlen=')) {
      meta.length = line.substring(6, line.length - 1)
    } else if (line.startsWith('source=')) {
      meta.source = line.substring(8, line.length - 1)
    }
  }

  // Return without duplicates
  return meta
}

function getHistory () {
  connection.connect(params)
    .then(prompt => {
      // Get history
      connection.send('output_archive.metadata', { maxBufferLength: '30M' })
        .then(res => {
          const hist = parseLiquidHistory(res)
          console.log(hist)
        })
    }, error => {
      console.log('promises reject:', error)
    })
    .catch(error => {
      console.log('Err:', error)
    })
}

getHistory()
