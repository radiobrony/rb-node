# RB-Node
> Websocket stuff I guess

```bash
# deps
yarn

# config
cp .env.example .env
$EDITOR .env

# live page
echo '<b>Rien pour le moment !' > live.html

# run
node index.js
```
