const fs = require('fs')
const path = require('path')

const yaml = require('js-yaml')
const axios = require('axios').default
const hmacSHA1 = require('crypto-js/hmac-sha1')
const Base64 = require('crypto-js/enc-base64')
const xmlescape = require('xml-escape')
const express = require('express')
const bodyParser = require('body-parser')
const WebSocket = require('ws')
const { createServer } = require('http')
const { Telnet } = require('telnet-client')
const { getAudioDurationInSeconds } = require('get-audio-duration')

// Load config
let config = yaml.load(fs.readFileSync('config.yml', 'utf8'))

// Init vars
let fsTimeout = null
const data = {
  timestamp: 0,
  tracks: {
    previous: {},
    current: {
      source: null,
      title: {
        artist: config.defaults.artist,
        title: config.defaults.title
      },
      duration: {
        string: '00:00',
        seconds: 0
      },
      start: {
        string: '00:00:00',
        ts: 0
      },
      end: {
        string: '00:00:00',
        ts: 0
      }
    },
    next: {},
    upcoming: []
  },
  meta: {
    cover: config.defaults.logo,
    covers: {
      orig: imgsign(config.defaults.logo),
      small: imgsign('0x140/' + config.defaults.logo),
      medium: imgsign('0x280/' + config.defaults.logo),
      large: imgsign('0x420/' + config.defaults.logo)
    },
    status: null,
    listeners: 9001,
    team: config.team
  },
  live: {
    html: '<i>Chargement...</i>',
    announce: null
  }
}
const telnetParams = {
  host: config.conn.LS_HOST,
  port: config.conn.LS_PORT,
  negotiationMandatory: false,
  timeout: 1500
}
let lastTitle = ''
let lastLiveTitle = ''
let lastLiveTS = 0

// --- Init objects ---
const jsonParser = bodyParser.json({ limit: '50mb' })
const connection = new Telnet()

// --- Connect ---
// Start webserver
const app = express()
app.use(express.static(path.join(__dirname, '/public')))

// Our web requests
app.post('/login', jsonParser, (req, res) => {
  const isOK = config.logins.some(login => {
    return login.username === req.body.user &&
           login.password === req.body.password
  })

  if (isOK) res.status(200).send('OK')
  else res.status(401).send('KO')
})
app.post('/np', jsonParser, (req, res) => {
  if (req.get('X-Pass') === config.conn.POST_PASS) {
    saveNP(req.body)
    res.send('OK')
  } else res.status(401).send('KO')
})
app.post('/next', jsonParser, (req, res) => {
  if (req.get('X-Pass') === config.conn.POST_PASS) {
    saveNext(req.body)
    res.send('OK')
  } else res.status(401).send('KO')
})
app.post('/live', jsonParser, (req, res) => {
  if (req.get('X-Pass') === config.conn.POST_PASS) {
    data.live.announce = req.body.announce
    data.live.html = req.body.html
    fs.writeFileSync('./public/live.html', req.body.html, 'utf-8')
    pushUpdates()
    res.send('OK')
  } else res.status(401).send('KO')
})
app.get('/admin', (_, res) => {
  res.redirect(301, '/admin/panel')
})
app.get('/nextsongs', (_, res) => {
  let outxml = `<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>
<SHOUTCASTSERVER>
  <NEXTSONGS>`

  data.tracks.upcoming.forEach((t, i) => {
    outxml += `
      <TITLE seq="${i + 1}">${xmlescape(t.title.artist)} - ${xmlescape(t.title.title)}</TITLE>`
  })

  outxml += `
  </NEXTSONGS>
</SHOUTCASTSERVER>`

  res.header('Content-Type', 'application/xml')
  res.status(200).send(outxml)
})

// Start websocket
const server = createServer(app)
const wss = new WebSocket.Server({ server })

// Hey, listen!
server.listen(config.conn.PORT, _ => {
  console.log(`Listening on :${config.conn.PORT}`)
})

// --- Functions ---
function imgsign (path) {
  try {
    const hash = hmacSHA1(path, config.secret)
    const bHash = Base64.stringify(hash).replace(/\+/g, '-').replace(/\//g, '_')
    return 'https://t.radiobrony.fr/' + bHash + '/' + path
  } catch (error) {
    console.log('Could not sign image.', error)
    return 'https://t.radiobrony.fr/unsafe/' + path
  }
}

function noop () {}

function heartbeat () {
  this.isAlive = true
}

function grabLength (filename) {
  const file = filename.replace('/library', config.paths.library)
  return getAudioDurationInSeconds(file)
    .then((duration) => {
      return duration * 1000
    })
    .catch(_ => {
      return null
    })
}

function sec2dur (seconds) {
  let outStr = 'LIVE'
  if (seconds) {
    const m = Math.floor(seconds / 60)
    const s = Math.floor(seconds % 60)
    outStr = m + ':' + (s < 10 ? '0' + s : s)
  }

  return {
    string: outStr,
    seconds
  }
}

function timing (hour, off) {
  const ts = hour.getTime()
  let str = 'LIVE'

  if (ts) {
    if (off) hour.setSeconds(hour.getSeconds() + off)

    str = hour.getHours() + ':'
    str += (hour.getMinutes() < 10 ? '0' + hour.getMinutes() : hour.getMinutes()) + ':'
    str += hour.getSeconds() < 10 ? '0' + hour.getSeconds() : hour.getSeconds()
  }

  return {
    string: str,
    ts
  }
}

function parseLiquidJSON (ls) {
  const j = {}
  ls.forEach(a => {
    j[a[0]] = a[1]
  })
  return j
}

function sourceType (source) {
  if (source === 'harbor_live') return 'LIVE'
  else if (source === 'request_shows') return 'EMISSION'
  else if (source === 'request_infos') return 'INFOS'
  else if (source === 'music_pl') return null
  return 'LIVE'
}

async function saveNP (body, forceLive) {
  const curr = parseLiquidJSON(body)

  if (Object.keys(curr).length === 0 && !forceLive) {
    // If we get an empty array, ask liquidsoap about the live mount
    setTimeout(_ => {
      axios.get(config.conn.LS_WEB + '/get_meta_live')
        .then(res => {
          const d = parseLiquidJSON(res.data)
          if (Object.keys(d).length === 0) return

          const liveTitle = d.title.replace('"', '\'')

          // If it's the same as the previous live title, it might be a residue
          // Instead ask for the current output title
          if (lastLiveTitle === liveTitle) {
            fetchNP(true)
            return
          }
          lastLiveTitle = liveTitle

          // If there's something, force it as the new title
          setTimeout(_ => {
            connection.connect(telnetParams)
              .then(_ => {
                connection.send('radio_meta.insert title="' + liveTitle + '"')
                  .then(rez => {
                    console.log('Set new title:', rez)
                  })
              }, error => {
                console.log('promises reject:', error)
              })
              .catch(error => {
                console.log('Err:', error)
              })
          }, 2500)
        })
    }, 1000)
  } else if ('title' in curr) {
    const start = new Date() // new Date(curr.on_air_timestamp * 1000)
    const src = sourceType(curr.source)

    if (src !== 'LIVE' && lastLiveTS > start.getTime() - 5000) return
    else if (src === 'LIVE') lastLiveTS = start.getTime()

    if (src === null && lastTitle === curr.title) return
    lastTitle = curr.title

    let length = null
    if ('tlen' in curr) {
      // If tags have length, use it
      length = parseInt(curr.tlen, 10)
    } else if ('filename' in curr) {
      // Otherwide go read the file if possible
      const grabbedLength = await grabLength(curr.filename)
      length = parseInt(grabbedLength, 10)
    }

    const end = new Date(start.getTime() + length)

    if (!('artist' in curr)) {
      const s = curr.title.split(/ - (.+)/)
      curr.artist = s[0]
      curr.title = s[1]
    }

    data.tracks.current = {
      source: src,
      title: {
        artist: curr.artist,
        title: curr.title
      },
      duration: sec2dur(length / 1000),
      start: timing(start),
      end: timing(end)
    }

    data.meta.status = src

    // Remove from the upcoming list
    const filterTrack = (el) => {
      return el.title.artist !== curr.artist &&
             el.title.title !== curr.title
    }

    data.tracks.upcoming = data.tracks.upcoming.filter(filterTrack)

    pushUpdates()
  }
}

async function saveNext (body) {
  const up = parseLiquidJSON(body)

  if (Object.keys(up).length > 0 && 'title' in up) {
    let length = null

    if ('tlen' in up) {
      // If tags have length, use it
      length = parseInt(up.tlen, 10)
    } else if ('filename' in up) {
      // Otherwide go read the file if possible
      const grabbedLength = await grabLength(up.filename)
      length = parseInt(grabbedLength, 10)
    }

    if (!('artist' in up)) {
      const s = up.title.split(/ - (.+)/)
      up.artist = s[0]
      up.title = s[1]
    }

    const hasTrack = (el) => {
      return el.title.artist === up.artist &&
             el.title.title === up.title
    }

    if (!data.tracks.upcoming.some(hasTrack)) {
      data.tracks.upcoming.push({
        title: {
          artist: up.artist,
          title: up.title
        },
        duration: sec2dur(length / 1000)
      })
      pushUpdates()
    }
  }
}

function fetchNP (forced) {
  axios.get(config.conn.LS_WEB + '/get_meta')
    .then(res => saveNP(res.data, forced))
    .catch(err => {
      console.log('Could not get now playing data', err)
    })
}

// Read live file
function updateHTML () {
  fs.readFile('./public/live.html', 'utf8', (err, contents) => {
    if (err) throw err

    if (contents) {
      data.live.html = contents
      pushUpdates()
    }
  })
}

// Check which logo to display
function checkLogo () {
  const now = Date.now()
  const prevCover = data.meta.cover

  const l = config.logos
  for (let i = 0; i < l.length; i++) {
    const s = new Date(Date.parse(l[i].start))
    const e = new Date(Date.parse(l[i].end))

    if (now >= s.getTime() && now < e.getTime()) {
      data.meta.cover = l[i].file
    }
  }

  data.meta.covers = {
    orig: imgsign(data.meta.cover),
    small: imgsign('0x140/' + data.meta.cover),
    medium: imgsign('0x280/' + data.meta.cover),
    large: imgsign('0x420/' + data.meta.cover)
  }

  if (prevCover !== data.meta.cover) pushUpdates()
}

// Send WS to everyone
function pushUpdates () {
  data.timestamp = Date.now()

  wss.clients.forEach(client => {
    try {
      if (client.readyState === WebSocket.OPEN) {
        client.send(JSON.stringify(data))
      }
    } catch (err) {
      console.log('Error when pushing update to client.', err)
    }
  })
}

// --- The rest to run ---
// Watch for config changes
fs.watch('config.yml', _ => {
  config = yaml.load(fs.readFileSync('config.yml', 'utf8'))
})

// Watch for live change
// fs.watch('./public/live.html', updateHTML)

// Check for logo changes every minute
setInterval(checkLogo, 60000)

// On WS connection
wss.on('connection', (ws, req) => {
  // const ip = req.headers['x-forwarded-for']?.split(',')[0].trim() || req.socket.remoteAddress

  ws.send(JSON.stringify(data))

  ws.on('message', (data, isBinary) => {
    const message = isBinary ? data : data.toString()

    if (message === 'get') {
      ws.send(JSON.stringify(data))
    } else if (message === 'srvtime') {
      const tsnow = { ts: Date.now() }
      ws.send(JSON.stringify(tsnow))
    }
  })

  ws.isAlive = true
  ws.on('pong', heartbeat)
})

// Close dead WS connections
setInterval(_ => {
  wss.clients.forEach(ws => {
    try {
      if (ws.isAlive === false) {
        return ws.terminate()
      }

      ws.isAlive = false
      ws.ping(noop)
    } catch (err) {
      console.log('Error on checking deads.', err)
    }
  })
}, 30000)

checkLogo()
updateHTML()
fetchNP()
